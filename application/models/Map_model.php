<?php
/**
 * Created by PhpStorm.
 * User: Rom4ess
 * Date: 20.12.2015
 * Time: 21:37
 */
class Map_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function get_info() {
        $this->db->select('buildings.name as build_name');
        $this->db->select('buildings.id as build_id');
        $this->db->from('buildings');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_floors() {
        $this->db->select('floors.id as f_id');
        $this->db->select('floors.flr_number as f_number');
        $this->db->select('floors.build_f_id as b_id');
        $this->db->select('floors.image_path as f_map');
        $this->db->from('floors');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_areas(){
        $this->db->select('objects.id as id');
        $this->db->select('objects.obj_name as name');
        $this->db->select('objects.obj_area as coords');
        $this->db->from('objects');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_coords($f_id = FALSE){
        $this->db->select('nodes.x_coord as x');
        $this->db->select('nodes.y_coord as y');
        $this->db->select('nodes.obj_id as area');
        $this->db->from('nodes');
        if($f_id !== FALSE)
            $this->db->where('nodes.floor_f_id',$f_id);
        $query = $this->db->get();
        return $query->result_array();
    }
}