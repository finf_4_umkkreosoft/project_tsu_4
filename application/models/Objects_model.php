<?php
class Objects_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
        $this->load->model('buildings_model');
        $this->load->model('floors_model');
    }

    public function get_objects($building_name = FALSE, $floor_number = FALSE, $obj_name = FALSE)
    {
        //if ($obj_name === FALSE and $building_name === FALSE and $floor_number === FALSE)
        //{
        //    $query = $this->db->get('objects');
        //    return $query->result_array();
        //}

        $this->db->select('o.obj_name');
        $this->db->select('f.flr_number');
        $this->db->select('b.name as build_name');
        $this->db->from('objects as o');
        $this->db->join('nodes as n','n.obj_id = o.id');
        $this->db->join('floors as f','n.floor_f_id = f.id', 'left');
        $this->db->join('buildings as b','f.build_f_id = b.id');

        if ($obj_name !== FALSE)
            $this->db->like('o.obj_name',$obj_name);

        if ($floor_number !== FALSE and $floor_number != 0)
            $this->db->where('f.flr_number',$floor_number);

        if ($building_name !== FALSE)
            $this->db->like('b.name',$building_name);





            //$this->db->like('obj_name', $obj_name);
        $query = $this->db->get();
        $result = $query->result_array();

        return $result;

    }
}