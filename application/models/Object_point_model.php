<?php
class Object_point_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function set_object_point()
    {
        $this->load->helper('url');

        $data = array(
            'name' => $this->input->post('name'),
            'x_coord' => $this->input->post('x_coord'),
            'y_coord' => $this->input->post('y_coord'),
            'obj_ar_id' => $this->input->post('obj_ar_id')
        );

        return $this->db->insert('object_points', $data);
    }

    public function get_object_point($id = FALSE)
    {
        if ($id === FALSE)
        {
            $query = $this->db->get('object_points');
            return $query->result_array();
        }

        $query = $this->db->get_where('object_points', array('id' => $id));
        return $query->row_array();
    }

    public function delete_object_point($id = FALSE)
    {
        $this->load->helper('url');
        $this->db->delete('object_points', array('id' => $id));
    }

    public function update_object_point($id = FALSE)
    {
        $this->load->helper('url');

        $data = array(
            'name' => $this->input->post('name'),
            'x_coord' => $this->input->post('x_coord'),
            'y_coord' => $this->input->post('y_coord'),
            'obj_ar_id' => $this->input->post('obj_ar_id')
        );

        $this->db->where('id', $id);
        $this->db->update('object_points', $data);
    }
}