<?php
class Buildings_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function get_buildings($name = FALSE)
    {
        if ($name === FALSE)
        {
            $query = $this->db->get('buildings');
            return $query->result_array();
        }

        $query = $this->db->get_where('buildings', array('name' => $name));
        return $query->row_array();
    }
}