<?php
class Object_areas_model extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function set_object_areas()
    {
        $this->load->helper('url');

        $data = array(
            'name' => $this->input->post('name'),
            'sockets' => $this->input->post('sockets'),
            'people' => $this->input->post('people'),
            'floor_id' => $this->input->post('floor_id')
        );

        return $this->db->insert('object_areas', $data);
    }

    public function get_object_areas($id = FALSE)
    {
        if ($id === FALSE)
        {
            $query = $this->db->get('object_areas');
            return $query->result_array();
        }

        $query = $this->db->get_where('object_areas', array('id' => $id));
        return $query->row_array();
    }

    public function delete_object_areas($id = FALSE)
    {
        $this->load->helper('url');
        $this->db->delete('object_areas', array('id' => $id));
    }

    public function update_object_areas($id = FALSE)
    {
        $this->load->helper('url');

        $data = array(
            'name' => $this->input->post('name'),
            'sockets' => $this->input->post('sockets'),
            'people' => $this->input->post('people'),
            'floor_id' => $this->input->post('floor_id')
        );

        $this->db->where('id', $id);
        $this->db->update('object_areas', $data);
    }
}