<?php
class Floors_model extends CI_Model {

    public function __construct()
    {
        $this->load->database();
    }

    public function get_floors($flr_number = FALSE)
    {
        if ($flr_number === FALSE)
        {
            $query = $this->db->get('floors');
            return $query->result_array();
        }

        $query = $this->db->get_where('floors', array('flr_number' => $flr_number));
        return $query->row_array();
    }
}