
<div class="container">


   <div class="row">
      <div class="col-md-4 color-swatches gray-lighter">
         <?php echo form_open('objects'); ?>
         <h2><?php echo $title; ?></h2>
         <div class="form-group">
            <label for="building">Название здания</label>
            <input type="text" name="building" class="form-control" value="<?php echo set_value('building'); ?>" /><br />
         </div>
         <div class="form-group">
            <label for="floor">Этаж</label>
            <input type="number" name="floor" class="form-control" value="<?php echo set_value('floor'); ?>" /><br />
         </div>
         <div class="form-group">
            <label for="object">Название или номер аудитории</label>
            <input type="text" name="object" class="form-control" value="<?php echo set_value('object'); ?>" /><br />
         </div>
         <div class="form-group">
            <input type="submit" class="btn btn-success" name="submit" value="Найти" />
         </div>

         </form>
      </div>
      <div class="col-md-8">
         <h3>Результат</h3>
         <div class="row">
            <?foreach ($objects as $object)
            {?>
               <div class="col-md-4">
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <h3 class="panel-title">Объект <?=$object["obj_name"];?></h3>
                     </div>
                     <div class="panel-body">
                        <dl>
                           <dt>Название помещения</dt>
                           <dd><?=$object['build_name'];?></dd>
                        </dl>
                        <dl>
                           <dt>Этаж</dt>
                           <dd><?=$object['flr_number'];?></dd>
                        </dl>
                        <dl>
                           <dt>Название/номер аудитории</dt>
                           <dd><?=$object['obj_name'];?></dd>
                        </dl>
                     </div>
                  </div>
               </div>
            <?}?>
         </div>
      </div>
   </div>
</div>

