<script src="/assets/js/jquery.js" type="text/javascript"></script>
<link href="/assets/css/map-search.css" rel="stylesheet">
<div id="maincontainer">
    <div class="image">
        <img src="" name="floor_map" id="f_map" usemap="#fmap" alt="">
            <p>
                <map id="fl_map" name="fmap"></map>
            </p>
    </div>
    <div id="boxcontainer">
        <? foreach ($map_data as $data_item) {?>
            <div id="box">
                <h4><span><?=$data_item['build_name'] ?></span></h4>
                    <dl style="margin-left: 10px">
                        <?foreach($floors as $floor){
                            if($floor['b_id'] === $data_item['build_id']) {?>
                                <li><a onclick="get_id('<?=$floor['f_id']?>')" href="javascript:l_map ('<?=$floor['f_map']?>')">Этаж <?=$floor['f_number']?></a></li>
                        <?} }?>
                    <dl>
                </div>
            <?} ?>
    </div>
    <div id="textcontainer"></div>
    <script src="/assets/js/map.js" type="text/javascript"></script>
    <script src="/assets/js/ShowHide.js" type="text/javascript"></script>
    <script src="/assets/js/get_id.js" type="text/javascript"></script>
    <script src="/assets/js/list.js" type="text/javascript"></script>
    <script src="/assets/js/MoveBlock.js" type="text/javascript"></script>
</div>