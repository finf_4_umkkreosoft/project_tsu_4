<!-- Page Content -->
<div class="container">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo $title; ?>
            </h1>
        </div>
    </div>

    <?php foreach ($news as $news_item): ?>

        <div class="row">
            <div class="col-md-7">
                <a href="#">
                    <img class="img-responsive" src="http://placehold.it/700x300" alt="">
                </a>
            </div>
            <div class="col-md-5">
                <h3><?php echo $news_item['title']; ?></h3>
                <p><?php echo $news_item['text']; ?></p>
                <a class="btn btn-primary" href="<?php echo '/news/'.$news_item['slug']; ?>">View Project <span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
        </div>

        <hr>

    <?php endforeach; ?>

</div>

