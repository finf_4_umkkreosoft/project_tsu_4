<!-- Page Content -->
<div class="container">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"><?php echo $title; ?>
            </h1>
        </div>
    </div>
    <!-- Панель инструментов администратора -->
    <ul class="nav nav-pills" id="admin_tools">
        <li role="presentation"><button class="btn btn-success add_data" type="submit">Добавить</button></li>

        <li role="presentation"><button class="btn btn-default file_upload_target" type="submit">Загрузить план</button></li>

        <li role="presentation"><button class="tool auditory btn btn-default" type="submit">Аудитория</button></li>

        <li role="presentation"><button class="tool dot_area btn btn-default" type="submit">Точечная область</button></li>

        <li role="presentation"><button class="tool ladder btn btn-default" type="submit">Лестница</button></li>

        <li role="presentation"><button class="tool corridore btn btn-default" type="submit">Коридор</button></li>

        <li role="presentation"><button class="btn btn-danger del_data" type="submit">Удалить</button></li>
        <li role="presentation"><button class="btn info btn-info"
                                        data-toggle="collapse"
                                        data-target="#info_dropdown"
                                        type="button">Свойства</button>
            <? // Тут выводятся своиства элемента ?>
            <div id="info_dropdown" class="dropdown collapse">
                <div class="panel panel-default">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <form class="form-inline">
                                <div class="form-group">
                                    <label for="exampleInputName1">Название</label>
                                    <input type="text" class="form-control" id="exampleInputName1" placeholder="Кабинет 100">
                                </div>
                            </form>
                        </li>
                        <li class="list-group-item">
                            <form class="form-inline">
                                <div class="form-group">
                                    <label for="exampleInputName2">Размеры</label>
                                    <input type="text" class="form-control" id="exampleInputName2" placeholder="0,0,500,0,500,500,0,500">
                                </div>
                            </form>
                        </li>
                        <li class="list-group-item">
                            <form class="form-inline">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <label for="exampleInputName3">Есть вайфай</label>
                                        <input id="exampleInputName3" type="checkbox">
                                    </div>
                                </div>
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
    </ul>
    <hr>




    <!-- Контент -->
    <div class="row">
        <div class="col-md-3">
            <!-- Группа навигационной панели по зданиям и этажам -->
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Главный корпус
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <ul>
                                <li><a href="#">Этаж 1</a></li>
                                <li><a href="#">Этаж 2</a></li>
                                <li><a href="#">Этаж 3</a></li>
                                <li><a href="#">Этаж 4</a></li>
                                <li><a href="#">Этаж 5</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Корпус #2
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                        <div class="panel-body">
                            <ul>
                                <li><a href="#">Этаж 1</a></li>
                                <li><a href="#">Этаж 2</a></li>
                                <li><a href="#">Этаж 3</a></li>
                                <li><a href="#">Этаж 4</a></li>
                                <li><a href="#">Этаж 5</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Корпус #3
                            </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                            <ul>
                                <li><a href="#">Этаж 1</a></li>
                                <li><a href="#">Этаж 2</a></li>
                                <li><a href="#">Этаж 3</a></li>
                                <li><a href="#">Этаж 4</a></li>
                                <li><a href="#">Этаж 5</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-9">
            <!-- Группа карты и полилиний -->
            <div class="well" id="map_container">
                <img src="/images/001.jpg" name="floor_map" class="f_map" usemap="#fmap" alt="" >

                <map name="fmap">
                    <area id="m_area" onclick="" shape="poly" coords="0,0,250,0,250,250,0,250," href="" title="lol">
                    <area id="m_area" onclick="" shape="circle" coords="807, 926, 20" href="" title="lol">
                    <area id="m_area" onclick="" shape="circle" coords="900, 1044, 20" href="" title="lol">
                    <area id="m_area" onclick="" shape="circle" coords="1293, 408, 20" href="" title="lol">
                    <area id="m_area" onclick="" shape="circle" coords="1241, 1175, 20" href="" title="lol">
                </map>
            </div>
        </div>
    </div>

</div>


<!-- Скрытая форма для загрузки файла с картинкой -->
<form hidden action="#" id="upload">
    <input id="file_upload_input" name="userfile" type="file" />
</form>


<script src="/assets/js/admin.js" type="text/javascript" defer='defer'></script>

