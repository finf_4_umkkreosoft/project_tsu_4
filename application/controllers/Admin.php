<?php
class Admin extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['title'] = 'Панель администратора';

        $this->load->view('templates/header', $data);
        $this->load->view('admin/index', $data);
        $this->load->view('templates/footer');
    }

    public function auth()
    {
        $data['title'] = 'Войдите в систему';

        $this->load->view('templates/header', $data);
        $this->load->view('admin/auth', $data);
        $this->load->view('templates/footer');
    }
}