<?php
class Object_areas extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('object_areas_model');
        $this->load->helper('url_helper');
    }

    public function index()
    {
        $data['object_areas'] = $this->object_areas_model->get_object_areas();
        $data['title'] = 'All object areas';

        $this->load->view('templates/header', $data);
        $this->load->view('object_areas/index', $data);
        $this->load->view('templates/footer');
    }

    public function add()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Create an object item';

        $this->form_validation->set_rules('name', 'Title', 'required');
        $this->form_validation->set_rules('sockets', 'Sockets', 'required');
        $this->form_validation->set_rules('people', 'People', 'required');
        $this->form_validation->set_rules('floor_id', 'Floor', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('templates/header', $data);
            $this->load->view('object_areas/create');
            $this->load->view('templates/footer');

        }
        else
        {
            $this->load->view('templates/header', $data);
            $this->object_areas_model->set_object_areas();
            $this->load->view('object_areas/success');
            $this->load->view('templates/footer');
        }
    }

    public function view($id = NULL)
    {
        $data['object_areas_item'] = $this->object_areas_model->get_object_areas($id);

        if (empty($data['object_areas_item'])) {
            show_404();
        }

        $data['title'] = $data['object_areas_item']['name'];

        $this->load->view('templates/header', $data);
        $this->load->view('object_areas/view', $data);
        $this->load->view('templates/footer');
    }

    public function update($id = NULL)
    {
        $data['object_areas_item'] = $this->object_areas_model->get_object_areas($id);

        if (empty($data['object_areas_item'])) {
            show_404();
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Изменить объект';
        $data['id'] = $id;

        $this->form_validation->set_rules('name', 'Title', 'required');
        $this->form_validation->set_rules('sockets', 'Sockets', 'required');
        $this->form_validation->set_rules('people', 'People', 'required');
        $this->form_validation->set_rules('floor_id', 'Floor', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('templates/header', $data);
            $this->load->view('object_areas/update');
            $this->load->view('templates/footer');
        }
        else
        {
            $this->load->view('templates/header', $data);
            $this->object_areas_model->update_object_areas();
            $this->load->view('object_areas/success');
            $this->load->view('templates/footer');
        }
    }

    public function delete($id = NULL)
    {
        $data['object_areas_item'] = $this->object_areas_model->get_object_areas($id);

        if (empty($data['object_areas_item'])) {
            show_404();
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Удалить объект';
        $data['id'] = $id;

        if ($this->input->post('deleteit') != 1)
        {
            $this->load->view('templates/header', $data);
            $this->load->view('object_areas/delete', $data);
            $this->load->view('templates/footer');

        }
        else
        {
            $this->object_areas_model->delete_object_areas($id);
            $this->load->view('object_areas/success');
        }
    }
}