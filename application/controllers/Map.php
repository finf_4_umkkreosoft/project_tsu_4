<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Rom4ess
 * Date: 16.12.2015
 * Time: 21:37
 */
class Map extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        session_start();
        $this->load->model('map_model');
    }

    public function index()
    {
        $this->load->helper('html');
        $this->load->helper('url');

        $data['title'] = 'Поиск объекта на карте';
        $data['map_data'] = $this->map_model->get_info();
        $data['floors'] = $this->map_model->get_floors();
        $this->load->view('templates/header', $data);
        $this->load->view('objects/map_search', $data);
        $this->load->view('templates/footer');
    }

    public function areas()
    {
        $fl = $_POST['floor'];
        $objects['areas'] = $this->map_model->get_areas();
        $objects['coords'] = $this->map_model->get_coords($fl);
        $j_objects = json_encode($objects);
        echo $j_objects;
    }
}