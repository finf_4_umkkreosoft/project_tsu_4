<?php
class Objects extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('objects_model');
        $this->load->helper('url_helper');
    }

    public function index()
    {
        $this->load->helper('url');
        $this->load->helper('form');


        $data['title'] = 'Поиск объекта';

        // Reloading data for form
        $form_object = $this->input->post('object');
        $form_building = $this->input->post('building');
        $form_floor = $this->input->post('floor');

        if ($form_floor or $form_building or $form_object)
        {
            $data['objects'] = $this->objects_model->get_objects($form_building, $form_floor, $form_object);
        }
        else
        {
            // Выгрузка всех
            $data['objects'] = $this->objects_model->get_objects();
        }

        $this->load->view('templates/header', $data);
        $this->load->view('objects/search', $data);
        $this->load->view('templates/footer');



    }

    public function view($obj_name = NULL)
    {
        $data['news_item'] = $this->objects_model->get_objects($obj_name);

        if (empty($data['news_item'])) {
            show_404();
        }

        $data['title'] = $data['news_item']['title'];

        $this->load->view('templates/header', $data);
        $this->load->view('objects/view', $data);
        $this->load->view('templates/footer');
    }
}