<?php
class Object_point extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('object_point_model');
        $this->load->helper('url_helper');
    }

    public function index()
    {
        $data['object_points'] = $this->object_point_model->get_object_point();
        $data['title'] = 'All object points';

        $this->load->view('templates/header', $data);
        $this->load->view('object_point/index', $data);
        $this->load->view('templates/footer');
    }

    public function add()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Create an object item';

        $this->form_validation->set_rules('name', 'Title', 'required');
        $this->form_validation->set_rules('x_coord', 'X', 'required');
        $this->form_validation->set_rules('y_coord', 'Y', 'required');
        $this->form_validation->set_rules('obj_ar_id', 'Area', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('templates/header', $data);
            $this->load->view('object_point/create');
            $this->load->view('templates/footer');

        }
        else
        {
            $this->load->view('templates/header', $data);
            $this->object_point_model->set_object_point();
            $this->load->view('object_point/success');
            $this->load->view('templates/footer');
        }
    }

    public function update($id = NULL)
    {
        $data['object_points_item'] = $this->object_points_model->get_object_point($id);

        if (empty($data['object_points_item'])) {
            show_404();
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Изменить объект';
        $data['id'] = $id;

        $this->form_validation->set_rules('name', 'Title', 'required');
        $this->form_validation->set_rules('x_coord', 'X', 'required');
        $this->form_validation->set_rules('y_coord', 'Y', 'required');
        $this->form_validation->set_rules('obj_ar_id', 'Area', 'required');

        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('templates/header', $data);
            $this->load->view('object_point/update');
            $this->load->view('templates/footer');
        }
        else
        {
            $this->load->view('templates/header', $data);
            $this->object_point_model->update_object_point();
            $this->load->view('object_point/success');
            $this->load->view('templates/footer');
        }
    }

    public function view($id = NULL)
    {
        $data['object_points_item'] = $this->object_point_model->get_object_point($id);

        if (empty($data['object_points_item'])) {
            show_404();
        }

        $data['title'] = $data['object_points_item']['name'];

        $this->load->view('templates/header', $data);
        $this->load->view('object_point/view', $data);
        $this->load->view('templates/footer');
    }

    public function delete($id = NULL)
    {
        $data['object_points_item'] = $this->object_point_model->get_object_point($id);

        if (empty($data['object_points_item'])) {
            show_404();
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Удалить объект';
        $data['id'] = $id;

        if ($this->input->post('deleteit') != 1)
        {
            $this->load->view('templates/header', $data);
            $this->load->view('object_point/delete', $data);
            $this->load->view('templates/footer');

        }
        else
        {
            $this->object_point_model->delete_object_point($id);
            $this->load->view('object_point/success');
        }
    }
}