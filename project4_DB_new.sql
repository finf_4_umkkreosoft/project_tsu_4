-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 24, 2016 at 09:54 AM
-- Server version: 5.5.25
-- PHP Version: 5.2.12

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `project4_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `buildings`
--

CREATE TABLE IF NOT EXISTS `buildings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `buildings`
--

INSERT INTO `buildings` (`id`, `name`) VALUES
(1, 'Корпус №2'),
(2, 'Главный корпус'),
(3, 'Корпус №3');

-- --------------------------------------------------------

--
-- Table structure for table `edges`
--

CREATE TABLE IF NOT EXISTS `edges` (
  `node1_f_id` int(11) NOT NULL,
  `node2_f_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `floors`
--

CREATE TABLE IF NOT EXISTS `floors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flr_number` int(3) NOT NULL,
  `image_path` text NOT NULL,
  `build_f_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `floors`
--

INSERT INTO `floors` (`id`, `flr_number`, `image_path`, `build_f_id`) VALUES
(1, 1, '/images/001.jpg', 1),
(2, 2, '/images/002.jpg', 1),
(3, 3, '/images/003.jpg', 1),
(4, 1, '', 2),
(5, 2, '', 2),
(6, 3, '', 2),
(7, 1, '', 3),
(8, 2, '', 3),
(9, 3, '', 3);

-- --------------------------------------------------------

--
-- Table structure for table `nodes`
--

CREATE TABLE IF NOT EXISTS `nodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `x_coord` int(11) NOT NULL,
  `y_coord` int(11) NOT NULL,
  `obj_id` int(11) NOT NULL,
  `floor_f_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `nodes`
--

INSERT INTO `nodes` (`id`, `x_coord`, `y_coord`, `obj_id`, `floor_f_id`) VALUES
(1, 250, 500, 1, 1),
(5, 700, 800, 2, 1),
(9, 0, 0, 3, 1),
(10, 0, 0, 4, 1),
(11, 0, 0, 5, 1),
(12, 0, 0, 6, 1),
(13, 0, 0, 7, 4),
(14, 0, 0, 8, 4),
(15, 0, 0, 9, 4),
(16, 0, 0, 10, 4),
(17, 450, 900, 11, 2),
(18, 0, 0, 12, 1);

-- --------------------------------------------------------

--
-- Table structure for table `objects`
--

CREATE TABLE IF NOT EXISTS `objects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `obj_name` text NOT NULL,
  `obj_area` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `objects`
--

INSERT INTO `objects` (`id`, `obj_name`, `obj_area`) VALUES
(1, '103', '0,0, 500,0, 500,500, 0,500'),
(2, '103а', '600,600, 800,600, 800,800, 600,800'),
(3, '103б', NULL),
(4, '118', NULL),
(5, '104', NULL),
(6, '102', NULL),
(7, '103', NULL),
(8, '103а', NULL),
(9, '103б', NULL),
(10, '103в', NULL),
(11, '203', '0,0, 900,0, 900,400, 0,400'),
(12, 'Коридор', '500,500, 900,500, 900,550, 500,550');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
