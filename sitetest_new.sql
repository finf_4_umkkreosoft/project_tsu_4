-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 24, 2015 at 09:03 AM
-- Server version: 5.5.25
-- PHP Version: 5.2.12

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `testsite`
--

-- --------------------------------------------------------

--
-- Table structure for table `buildings`
--

CREATE TABLE IF NOT EXISTS `buildings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `buildings`
--

INSERT INTO `buildings` (`id`, `name`) VALUES
(1, 'Корпус №2'),
(2, 'Главный корпус'),
(3, 'Корпус №3');

-- --------------------------------------------------------

--
-- Table structure for table `floors`
--

CREATE TABLE IF NOT EXISTS `floors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flr_number` int(3) NOT NULL,
  `image_path` text NOT NULL,
  `build_f_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `floors`
--

INSERT INTO `floors` (`id`, `flr_number`, `image_path`, `build_f_id`) VALUES
(1, 1, '/images/001.jpg', 1),
(2, 2, '/images/002.jpg', 1),
(3, 3, '/images/003.jpg', 1),
(4, 1, '', 2),
(5, 2, '', 2),
(6, 3, '', 2),
(7, 1, '', 3),
(8, 2, '', 3),
(9, 3, '', 3);

-- --------------------------------------------------------

--
-- Table structure for table `objects`
--

CREATE TABLE IF NOT EXISTS `objects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `obj_name` text NOT NULL,
  `floor_f_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `objects`
--

INSERT INTO `objects` (`id`, `obj_name`, `floor_f_id`) VALUES
(1, '103', 1),
(2, '103а', 1),
(3, '103б', 1),
(4, '118', 1),
(5, '104', 1),
(6, '102', 1),
(7, '103', 4),
(8, '103а', 4),
(9, '103б', 4),
(10, '103в', 4);

-- --------------------------------------------------------

--
-- Table structure for table `object_areas`
--

CREATE TABLE IF NOT EXISTS `object_areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `sockets` int(11) NOT NULL,
  `people` text NOT NULL,
  `floor_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `object_areas`
--

INSERT INTO `object_areas` (`id`, `name`, `sockets`, `people`, `floor_id`) VALUES
(1, 'Пример', 1, 'люди', 1),
(2, 'Пример1', 2, 'people', 1);

-- --------------------------------------------------------

--
-- Table structure for table `object_points`
--

CREATE TABLE IF NOT EXISTS `object_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `x_coord` int(11) NOT NULL,
  `y_coord` int(11) NOT NULL,
  `obj_ar_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `object_points`
--

INSERT INTO `object_points` (`id`, `name`, `x_coord`, `y_coord`, `obj_ar_id`) VALUES
(1, '11', 0, 0, 1),
(2, '12', 500, 0, 1),
(3, '13', 500, 500, 1),
(4, '13', 0, 500, 1),
(5, '21', 600, 600, 2),
(6, '22', 800, 600, 2),
(7, '23', 800, 800, 2),
(8, '24', 600, 800, 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
