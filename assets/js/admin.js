(function($) {
    var toolsPanelItems = $("#admin_tools");
    var tools = $(toolsPanelItems).find(".tool");

    var uploadTarget = $(toolsPanelItems).find(".file_upload_target");
    var uploadInput = $("#file_upload_input");

    var addData = $(toolsPanelItems).find(".add_data");
    var delData = $(toolsPanelItems).find(".del_data");

    var map_container = $("#map_container");
    var map = $(map_container).find(".f_map");
    var poliMap = $(map_container).find("map");

    var dotsCounter = 0;
    var selectedTool = null;
    dots = [];

    // Стартовая надстройка
    reloadmapster();

    /*
    * reloadmapster () - Функция для обработки изменений в полилиниях
    */
    function reloadmapster () {
        $(map).maphilight({alwaysOn:true, fill:true, strokeWidth: 4, strokeColor: '000000'});
        rebindMap();
    }

    /*
    * reloadmapster () - Функция для обработки нажатий на карту, обернута в метод, чтобы,
    * в случае переинициализации DOM элемента карты обработчик оставался на своем месте
    */
    function rebindMap () {
        map.bind('click', function(e){
            var offset = $(this).offset();
            var xOffset = e.pageX - offset.left;
            var yOffset = e.pageY - offset.top;
            console.log("Click position x: " + xOffset + " y: " + yOffset);


            if (!selectedTool) {
                alert("Не выбран инструмент");
            } else {
                if (selectedTool == "auditory" || selectedTool == "ladder"){
                    // Отрисовка квадратных областей
                    if (dotsCounter < 1) {
                        dots.push([xOffset, yOffset]);
                        dotsCounter++;
                    }
                    else {
                        dots.push([xOffset, yOffset]);
                        var coords = dots[0][0] + "," + dots[0][1] + "," +
                            dots[1][0] + "," + dots[0][1] + "," +
                            dots[1][0] + "," + dots[1][1] + "," +
                            dots[0][0] + "," + dots[1][1];
                        console.log(coords);
                        dots = [];
                        dotsCounter = 0;
                        poliMap.append("<area id=\"m_area\" href='#' data-polytype=\"" + selectedTool + "\" shape=\"poly\" coords=\"" + coords + "\">");
                        reloadmapster();
                    }
                }
                else if (selectedTool == "dot_area") {
                    // Отрисовка точечных областей
                    var coords = xOffset + "," + yOffset + "," + 20;
                    poliMap.append("<area id=\"m_area\" href='#' data-polytype=\"" + selectedTool + "\" shape=\"circle\" coords=\"" + coords + "\">");
                    reloadmapster();
                }
                else {
                    // Отрисовка корридоров
                    if (dotsCounter < 1) {
                        dots.push([xOffset, yOffset]);
                        dotsCounter++;
                    }
                    else {
                        dots.push([xOffset, yOffset]);
                        var coords = dots[0][0] + "," + dots[0][1] + "," +
                            dots[1][0] + "," + dots[1][1];
                        console.log(coords);
                        dots = [];
                        dotsCounter = 0;
                        poliMap.append("<area id=\"m_area\" href='#' data-polytype=\"" + selectedTool + "\" shape=\"poly\" coords=\"" + coords + "\">");
                        reloadmapster();
                    }
                    var coords = xOffset + "," + yOffset + "," + 20;
                    poliMap.append("<area id=\"m_area\" href='#' data-polytype=\"" + selectedTool + "\" shape=\"poly\" coords=\"" + coords + "\">");
                    reloadmapster();
                }
            }
        });
    }

    /*
    * Обрабокта выбора инструмента для рисования полилиниями*/
    $(tools).click(function(ev){
        if (!$(ev.target).hasClass(selectedTool)){
            toolsPanelItems.find(".active").removeClass("active");
            $(ev.target).addClass("active");
            if ($(ev.target).hasClass("ladder"))
                selectedTool = "ladder";
            else if ($(ev.target).hasClass("auditory"))
                selectedTool = "auditory";
            else if ($(ev.target).hasClass("dot_area"))
                selectedTool = "dot_area";
            else if ($(ev.target).hasClass("corridore"))
                selectedTool = "corridore";

            console.log(selectedTool);
        } else {
            $(this).removeClass("active");
            selectedTool = null;
        }
    });

    $(uploadTarget).click(function(){
        $(uploadInput).trigger('click');
        $("#upload").submit();
    });

    $(addData).click(function(){
        alert("Что-то должно добавляться!");
    });

    $(delData).click(function(){
        alert("Что-то должно удаляться!");
    });

})(jQuery);