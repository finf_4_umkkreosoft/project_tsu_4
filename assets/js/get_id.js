/**
 * Created by Rom4ess on 22.12.2015.
 */
function get_id(fl){
    var on_click = "";
    var ref = "";
    var isArea = false;
    $.ajax({
        url: '/map/areas',
        type: 'POST',
        data: {'floor': fl},
        success: function(){console.log("Отправка прошла успешно.");},
        error: function(){console.log("При отправке произошла ошибка.");},
        complete: function(j_objects){
            var ars = $.parseJSON(j_objects.responseText);
            get_names(ars.areas);
            console.log(ars);
            $("map").empty();
            for(var i = 0; i<ars.areas.length; i=i+1){
                on_click = "";
                ref = "";
                isArea = false;
                for(var j = 0; j < ars.coords.length; j = j + 1)
                    if(ars.coords[j].area==ars.areas[i].id)
                        isArea = true;
                if(isArea){
                    if(ars.areas[i].name!="Коридор" && ars.areas[i].name!="Лестница"){
                        on_click = "onclick='get_click(window.event)'";
                        ref = "href='javascript: showHide(" + i + ")'";
                    }
                    if(ars.areas[i].coords!=null)
                        $("map").append("<area id='m_area' " + on_click + " shape='poly' coords='" + ars.areas[i].coords + "' " + ref + " >");
                }
            }
        }
    });
}